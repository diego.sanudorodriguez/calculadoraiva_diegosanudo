/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Calculadora;

import java.util.Scanner;

/**
 *
 * @author DAM127
 */
public class calcularIVA {

    static Scanner scanner = new Scanner(System.in);
    static int opcion = -1;
    static double cantidad;
    static double iva;
    static double calculo;

    public static void main(String[] args) {

        while (opcion != 0) {
            introducirDatos();
        }

    }

    private static void introducirDatos() {
        //Try catch para evitar que el programa termine si hay un error
        try {
            System.out.println("Elige opción:\n1.- Sumar IVA"
                    + "\n2.- Restar IVA\n"
                    + "0.- Salir");
            //Recoger una variable por consola
            opcion = Integer.parseInt(scanner.nextLine());
            switch (opcion) {
                case 1: {
                    System.out.println("Introducir precio sin IVA:");
                    cantidad = scanner.nextDouble();
                    System.out.println("Introducir porcentaje de IVA:");
                    iva = scanner.nextDouble();

                    calculo = cantidad + cantidad * iva / 100;

                    System.out.println("Precio base: " + cantidad);
                    System.out.println("IVA: " + iva);
                    System.out.println("Precio total: " + calculo);
                }

                case 2: {
                    System.out.println("Introducir precio final, IVA incluido:");
                    cantidad = scanner.nextDouble();
                    System.out.println("Introducir porcentaje de IVA:");
                    iva = scanner.nextDouble();

                    calculo = cantidad - cantidad * iva / 100;

                    System.out.println("Base: " + calculo);
                    System.out.println("IVA: " + iva);
                    System.out.println("Total: " + cantidad);
                }
                case 0: {
                    System.out.println("Saliendo del programa.");
                }
                default: {
                    System.out.println("Opción incorrecta. Selecciona una opción válida.");

                }
            }

            System.out.println("\n");

        } catch (NumberFormatException e) {
            System.out.println("Error!");
        }
    }
}
